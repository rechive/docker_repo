<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use App\Entity\LampGroups;
use App\Entity\LampsGroups;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Cache(expires="3 days", public=true)
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $groups = $entityManager->getRepository(LampGroups::class)->findAll();
        $lampsGroups = $entityManager->getRepository(LampsGroups::class)->findAll();

        $AllLamps = $entityManager->getRepository(\App\Entity\Lamps::class)->findAll();
        foreach($AllLamps as $lamp)
        {
            $mask['type'] = "FeatureCollection";
            $mask['features'][] =  array(
                "id"=> $lamp->GetID(),
                "type"=>"Feature",
                "geometry" => array (
                      "type" => "Point",
                      "coordinates" => [floatval($lamp->getPositionX()), floatval($lamp->getPositionY())]
                ),
                "data"=> [
                    "power"=> "on",
                    "brightness"=> 34
                ],
                "options"=>[
                    "balloonContentLayout"=>"my#baloonLayoutClass"
                ],
                "properties" => array (
                       "iconContent" => "Л".$lamp->GetID(),
                       "brightness"=>  $lamp->GetBrightness(),
                       "lamp_id"=> $lamp->GetID(),
                       //"group"=> $lamp->GetGroup()
                 )
            );
        }  

        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'groups'=>$groups,
            'LampsGroups'=>$lampsGroups,
            'AllLamps'=>$mask
        ]);
    }
}
